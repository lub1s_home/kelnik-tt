<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/autoload.php';
if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php';
}

use Lub1s\Import;
use Lub1s\Test;

if (! empty($_FILES) && ! empty($_REQUEST['submit_csv'])) {
    $objTest = new Test();
    Import::process($objTest);
    $allTestTable = $objTest->getAll();
}

include $_SERVER['DOCUMENT_ROOT'] . '/view/index.php';
