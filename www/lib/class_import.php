<?php
namespace Lub1s;

use Lub1s\Test;

class Import
{
    /**
     * @param \Lub1s\Test $objTest
     */
    public static function process(Test $objTest)
    {
        self::checkUploadFile();

        if (($handle = fopen($_FILES['csv']['tmp_name'], 'r')) !== false) {
            $i = 0;
            $arData = [];
            $resetCountArticle = [];
            while (($data = fgetcsv($handle, 1000, ';')) !== false) {
                $i++;
                if (count($data) !== 3) {
                    die('Некорректный scv-файл, неправильное количество ячеек на строке ' . $i);
                }
                $arData[] = $data;
                $resetCountArticle[] = $data[0];
            }

            foreach ($arData as $data) {
                $article = $data[0];
                $price = (int) $data[1];
                $count = (int) $data[2];

                if ($id = $objTest->getIdByArticle($article)) {
                    $objTest->update($id, $price, $count);
                } else {
                    $objTest->insert($article, $price, $count);
                }
            }

            $objTest->resetCount($resetCountArticle);
        }
    }

    private static function checkUploadFile()
    {
        if ($_FILES['csv']['error']) {
            die('Произошла ошибка при загрузке файла');
        }
        if (pathinfo($_FILES['csv']['name'], PATHINFO_EXTENSION) !== 'csv') {
            die('Файл должен быть в формате csv');
        }
    }
}
