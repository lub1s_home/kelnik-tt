<?php
namespace Lub1s;

class Test extends Common
{
    /**
     * @return array
     */
    public function getAll()
    {
        $stmt = $this->pdo->prepare('SELECT * FROM ' . $this->tableName . ' ORDER BY `ID`');
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param string $article
     * @return int|false
     */
    public function getIdByArticle(string $article)
    {
        $stmt = $this->pdo->prepare('SELECT `ID` FROM ' . $this->tableName . ' WHERE `ARTICUL` = ?');
        $stmt->bindParam(1, $article);
        $stmt->execute();

        if ($result = $stmt->fetch()) {
            $result = (int) $result['ID'];
        }

        return $result;
    }

    /**
     * @param int $id
     * @param int $price
     * @param int $count
     * @return bool
     */
    public function update(int $id, int $price, int $count)
    {
        $stmt = $this->pdo->prepare(
            'UPDATE ' . $this->tableName . ' SET `PRICE` = ?, `COUNT` = ? WHERE `ID` = ?'
        );
        $bindData = [
            $price,
            $count,
            $id,
        ];
        return $stmt->execute($bindData);
    }

    /**
     * @param string $article
     * @param int $price
     * @param int $count
     * @return bool
     */
    public function insert(string $article, int $price, int $count)
    {
        $stmt = $this->pdo->prepare(
            'INSERT INTO ' . $this->tableName . '(`ARTICUL`, `PRICE`, `COUNT`) VALUES (?, ?, ?)'
        );
        $bindData = [
            $article,
            $price,
            $count,
        ];
        return $stmt->execute($bindData);
    }

    public function resetCount(array $articles)
    {
        if (empty($articles)) {
            return false;
        }

        $in  = str_repeat('?,', count($articles) - 1) . '?';
        $stmt = $this->pdo->prepare(
            'UPDATE ' . $this->tableName . ' SET `COUNT` = 0 WHERE `ARTICUL` NOT IN (' . $in . ')'
        );
        return $stmt->execute($articles);
    }
}
