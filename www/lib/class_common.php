<?php
namespace Lub1s;

class Common
{
    protected $pdo;
    protected $tableName;

    public function __construct()
    {
        $this->pdo = DataBase::getInstance();

        $arr = explode('\\', get_class($this));
        $this->tableName = mb_strtolower($arr[count($arr) - 1]);
    }
}
