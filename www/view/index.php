<?php

?><!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <title>Алексей Курза - тестовое задание</title>
</head>
<body>
    <section>
        <h1>Загрузите csv-файл</h1>
        <form action="/" enctype="multipart/form-data" method="post">
            <p><input type="file" name="csv"></p>
            <p><input type="submit" name="submit_csv" value="Импортировать"></p>
        </form>
    </section>
</body>
<?php if (! empty($allTestTable)) : ?>
    <table>
        <tr>
            <td>ID</td>
            <td>ARTICUL</td>
            <td>PRICE</td>
            <td>COUNT</td>
        </tr>
        <?php foreach ($allTestTable as $row) : ?>
            <tr>
                <td><?= $row['ID'] ?></td>
                <td><?= $row['ARTICUL'] ?></td>
                <td><?= $row['PRICE'] ?></td>
                <td><?= $row['COUNT'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
